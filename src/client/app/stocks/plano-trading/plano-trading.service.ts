import {DataOperacao} from "../operacao/data-operacao";
import {Ativo} from "../ativo/ativo";
import {Injectable} from '@angular/core';
import {PlanoTrading} from "./plano-trading";

@Injectable()
export class PlanoTradingService {
  planos:PlanoTrading[];

  constructor(){
    let plano:PlanoTrading = new PlanoTrading();
    plano.tipo = "Compra";
    plano.ativo = new Ativo("Petrobras", "PETR5");
    this.planos = [
      plano
    ]
  }

  obterTodosOsPlanos():PlanoTrading[]{
    return this.planos;
  }
}
