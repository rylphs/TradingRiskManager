import {EventMsg} from "../../shared/event/event-msg";
import {Ativo} from "../ativo/ativo";
import {DataOperacao} from "../operacao/data-operacao";
import {PlanoTrading} from "./plano-trading";
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'plano-trading',
    template: `
      <div  class="ui-grid ui-grid-responsive ui-fluid">
        <div class="ui-grid-row">
          <div class="ui-grid-col-3">Data:</div>
          <div class="ui-grid-col-9"><p-calendar dateFormat="dd/mm/yy" [(ngModel)]="dataPlano"></p-calendar></div>
        </div>

      <div class="ui-grid-row">
        <div class="ui-grid-col-3">Ativo:</div>
        <div class="ui-grid-col-9"><input pInputText [(ngModel)]="ativo"/></div>
      </div>

      <div class="ui-grid-row">
        <div class="ui-grid-col-3">Preco:</div>
        <div class="ui-grid-col-9"><input pInputText [(ngModel)]="preco"/></div>
      </div>

      <div class="ui-grid-row">
        <div class="ui-grid-col-3">Volume:</div>
        <div class="ui-grid-col-9"><p-spinner [min]="100" [(ngModel)]="volume" [step]="100"></p-spinner></div>
      </div>

      <div class="ui-grid-row">
        <div class="ui-grid-col">
          <button type="text" (click)="confirmarEdicao()" pButton icon="fa-check" label="Confirmar"></button>
        </div>
        <div class="ui-grid-col-12"></div>
        <div class="ui-grid-col">
          <button type="text" (click)="cancelarEdicao()"
            pButton icon="fa-close" label="Cancelar"></button>
        </div>
      </div>
    </div>
    `,
    styles: [`
      .ui-grid-row {
        padding: 10px;
      }
    `]
})

export class PlanoTradingComponent {
    private _plano: PlanoTrading;
    dataPlano: string = '';
    ativo: string;
    volume: number = 100;
    preco: number;
    mensagem:string = "Plano ativo '{{ativo}}' cadastrado com sucesso";

    @Input() set tipoOperacao(tipo:"EDICAO"|"INSERCAO"){
      if(tipo == "INSERCAO") return;

      this.mensagem = "Plano ativo '{{ativo}}' alterado com sucesso";
    }


    @Output() onConfirm: EventEmitter<EventMsg> =
    new EventEmitter<EventMsg>();

    @Input() set plano(plano: PlanoTrading) {
        this._plano = plano;
        if (plano) {
            this.dataPlano = plano.data.toString();
            this.ativo = plano.ativo.codigo;
            this.volume = plano.volume;
            this.preco = plano.preco;
        }
    }

    get plano(){
      return this._plano;
    }

    confirmarEdicao() {
        this._plano.data = new DataOperacao(this.dataPlano);
        this._plano.ativo = new Ativo("", this.ativo);
        this._plano.volume = this.volume;
        this._plano.preco = this.preco;
        this.onConfirm.emit(
            new EventMsg(this.mensagem, {status: "SUCESSO"})
        );
    }

    cancelarEdicao() {
        this.onConfirm.emit(
            new  EventMsg("Cancelado", {status: "CANCELADO"})
        );
    }
}
