import {EventMsg} from "../../shared/event/event-msg";
import {GrowlMsgService} from "../../shared/growl/growl-msg.service";
import {PlanoTrading} from "./plano-trading";
import {PlanoTradingService} from "./plano-trading.service";
import { Component} from '@angular/core';

@Component({
    selector: 'plano-trading-list',
    template: `
    <div (dblclick)="editarPlano()" class="no-select">
      <p-dataTable [value]="planos"
        selectionMode="single" [(selection)]="planoSelecionado">
          <p-column field="data" header="Data"></p-column>
          <p-column styleClass="non-select" field="ativo.codigo" header="Ativo"></p-column>
          <p-column field="volume" header="Volume"></p-column>
          <p-column field="preco" header="Preco"></p-column>
      </p-dataTable>
    </div>

    <button [disabled]="nenhumPlanoSelecionado" type="button" (click)="editarPlano()" pButton icon="fa fa-edit"></button>
    <button type="button" (click)="criarPlano()" pButton icon="fa fa-plus"></button>


    <p-dialog resizable="{{false}}" width="{{400}}" header="{{tradingPlanTitle}}" [(visible)]="showDialog">
      <plano-trading (onConfirm)="confirmarCadastro($event)" [plano]="planoEmEdicao" [tipoOperacao]="tipoOperacao"></plano-trading>
    </p-dialog>
    `,
})

export class PlanoTradingListComponent{
  planos: PlanoTrading[];
  showDialog:boolean = false;
  tradingPlanTitle = "Novo Plano";
  planoEmEdicao:PlanoTrading;
  tipoOperacao:  "EDICAO"|"INSERCAO" = "INSERCAO";


  constructor(planosService: PlanoTradingService, private msg:GrowlMsgService) {
    this.planos = planosService.obterTodosOsPlanos();
  }

  criarPlano(){
    this.planoEmEdicao = new PlanoTrading();
    this.tipoOperacao = "INSERCAO";
    this.tradingPlanTitle = "Novo Plano";
    this.showDialog = true;
  }

  set planoSelecionado(plano:PlanoTrading){
    this.planoEmEdicao = plano;
  }

  get planoSelecionado(){
    return this.planoEmEdicao;
  }

  get nenhumPlanoSelecionado():boolean{
    return this.planoEmEdicao == null;
  }

  confirmarCadastro(event:EventMsg){
    if(event.data.status == "SUCESSO"){
      if(this.tipoOperacao == "INSERCAO"){
        this.planos.push(this.planoEmEdicao);
      }
      this.msg.showMsg(event.getMsg({ativo: this.planoEmEdicao.ativo.codigo}));
    }
    this.planoEmEdicao = null;
    this.showDialog = false;
  }

  editarPlano() {
        this.tradingPlanTitle = "Editar Plano";
        this.tipoOperacao = "EDICAO";
        this.showDialog = true;
    }

    cancelSelect(evt:MouseEvent){
      evt.stopPropagation();
      evt.stopImmediatePropagation();
      evt.preventDefault();
    }

}
