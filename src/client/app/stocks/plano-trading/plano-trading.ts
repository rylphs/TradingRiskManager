import {AnaliseRisco} from "./analise.risco";
import {Operacao} from "../operacao/operacao";
import {Ativo, PrecoAtivo} from "../ativo/ativo";
import {DataOperacao} from "../operacao/data-operacao";
import * as moment from 'moment';

export type StatusPlano = "Aguardando" | "Executado" | "Liquidado" | "Stop" | "Cancelado";

export type TipoPlano = "Compra" | "Venda";

export class PlanoTrading {
  ativo: Ativo;
  data: DataOperacao;
  tipo: TipoPlano = "Compra";
  entrada: Operacao;
  saida: Operacao;
  status: StatusPlano;
  gatilho: PrecoAtivo;
  volume: number = 100;
  alvo: PrecoAtivo;
  stop: PrecoAtivo;
  analise: AnaliseRisco;
  preco: number = 0;

  constructor(){
    this.data = new DataOperacao();
    this.ativo = new Ativo("","");
  }
}
