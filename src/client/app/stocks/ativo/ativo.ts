export class Ativo {
  constructor(public nome:string, public codigo:string){}
}

export type PrecoAtivo = {
  abertura:number;
  fechamento:number;
  minima:number;
  baixa:number;
}
