import {DataOperacao} from "./data-operacao";
import {Ativo} from "./ativo";

export type TipoOperacao  = "ENTRADA" | "SAIDA"

export class Operacao{
  tipo: TipoOperacao;
  valor: number;
  volume: number;
  data: DataOperacao;
  ativo: Ativo;
}
