import * as moment from 'moment';

const DATA_FORMAT = "DD/MM/YYYY";

export class DataOperacao {
  private data: moment.Moment;

  constructor(dataStr?:string){
    if(!dataStr) this.data = moment();
    else this.data = moment(dataStr, DATA_FORMAT);
  }

  toString(){
    return this.data.format(DATA_FORMAT);
  }
}
