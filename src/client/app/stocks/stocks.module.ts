import {PlanoTradingComponent} from "./plano-trading/plano-trading.component";
import {PlanoTradingService} from "./plano-trading/plano-trading.service";
import {PlanoTradingListComponent} from "./plano-trading/plano-trading-list.component";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DataTableModule} from 'primeng/components/datatable/datatable';
import {InputTextModule} from 'primeng/components/inputtext/inputtext';
import {DialogModule} from 'primeng/components/dialog/dialog';
import {SharedModule} from 'primeng/components/common/shared';
import {ButtonModule} from 'primeng/components/button/button';
import {CalendarModule} from 'primeng/components/calendar/calendar';
import {SpinnerModule} from 'primeng/components/spinner/spinner';
import {FormsModule} from '@angular/forms';


@NgModule({
    imports: [CommonModule, DataTableModule,SharedModule, DialogModule,
      InputTextModule, ButtonModule, SpinnerModule, CalendarModule, FormsModule],
    declarations: [PlanoTradingListComponent, PlanoTradingComponent],
    exports: [PlanoTradingListComponent, PlanoTradingComponent],
    providers: [PlanoTradingService],
})

export class StocksModule { }
