export class EventMsg{
    constructor(
      public description: string,
      public data?: any) { }

    getMsg(params?: any) {
        if (!params) return this.description;
        //TODO implementar escape char
        let msg = this.description.replace(/([^{])\{\{([^}{]+)\}\}/,
            function(matched: string, p1: string, p2: string, offset: number) {
                if (params[p2]) return p1 + params[p2];
                return matched;
            }
        );
        return msg;
    }
}
