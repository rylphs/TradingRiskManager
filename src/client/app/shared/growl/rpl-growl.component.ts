import {GrowlMsgService} from "./growl-msg.service";
import { Component} from '@angular/core';
import {Message} from 'primeng/components/common/api';

@Component({
    selector: 'rpl-growl',
    template: `<p-growl [value]="msgs"></p-growl>`,
})

export class RplGrowlComponent{
  private msgs:Message[];

  constructor(private msg:GrowlMsgService) {
    this.msgs = msg.getMsgs();
  }

}
