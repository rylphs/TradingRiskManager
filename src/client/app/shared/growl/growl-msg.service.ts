import {Injectable} from '@angular/core';
import {Message} from 'primeng/components/common/api';

@Injectable()
export class GrowlMsgService{
  private msgs:Message[] = new Array();

  showMsg(msg:string){
    this.msgs.push({summary:msg});
  }

  getMsgs(){
    return this.msgs;
  }
}
