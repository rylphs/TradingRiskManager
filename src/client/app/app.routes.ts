import {PlanoTradingListComponent} from "./stocks/plano-trading/plano-trading-list.component";
import { Routes } from '@angular/router';


export const routes: Routes = [
  {
    path: '',
    component: PlanoTradingListComponent
  },
];
