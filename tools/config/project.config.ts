import { join } from 'path';

import { SeedConfig } from './seed.config';

/**
 * This class extends the basic seed configuration, allowing for project specific overrides. A few examples can be found
 * below.
 */
export class ProjectConfig extends SeedConfig {

  PROJECT_TASKS_DIR = join(process.cwd(), this.TOOLS_DIR, 'tasks', 'project');
  FONTS_DEST = `${this.APP_DEST}/font-awesome/fonts`;
  FONTS_SRC = ['node_modules/font-awesome/fonts/**',
    '${this.CSS_SRC}/theme/layout/fonts']; //Modena theme

  constructor() {
    super();
    // this.APP_TITLE = 'Put name of your app here';

    /* Enable typeless compiler runs (faster) between typed compiler runs. */
    // this.TYPED_COMPILE_INTERVAL = 5;

    // Add `NPM` third-party libraries to be injected/bundled.
    this.NPM_DEPENDENCIES = [
      ...this.NPM_DEPENDENCIES,
      //{src: 'primeui/themes/flick/theme.css', inject: true},
      {src: 'primeui/primeui-ng-all.min.js', inject: true},
      {src: 'moment/min/moment-with-locales.min.js', inject: 'libs'},
      {src: 'font-awesome/css/font-awesome.min.css', inject: true}
      // {src: 'jquery/dist/jquery.min.js', inject: 'libs'},
      // {src: 'lodash/lodash.min.js', inject: 'libs'},
    ];


    // Add `local` third-party libraries to be injected/bundled.
    this.APP_ASSETS = [
      ...this.APP_ASSETS,
      { src: `${this.CSS_SRC}/main.${ this.getInjectableStyleExtension() }`, inject: true, vendor: false },
    //  { src: `${this.CSS_SRC}/screen.${ this.getInjectableStyleExtension() }`, inject: true, vendor: false },

      //Modena theme
      { src: `${this.CSS_SRC}/theme/theme/theme.css`, inject: true, vendor: false },
      { src: `${this.CSS_SRC}/theme/layout/css/core-layout.css`, inject: true, vendor: false },
      { src: `${this.CSS_SRC}/theme/layout/css/animate.css`, inject: true, vendor: false },
      { src: `${this.CSS_SRC}/theme/layout/css/modena-font.css`, inject: true, vendor: false },
      { src: `${this.CSS_SRC}/theme/layout/css/ripple-effect.css`, inject: true, vendor: false },
      { src: `${this.CSS_SRC}/theme/layout/css/perfect-scrollbar.css`, inject: true, vendor: false },
      { src: `${this.CSS_SRC}/theme/layout/css/modena-layout.css`, inject: true, vendor: false },
      {src: 'node_modules/primeui/primeui-ng-all.min.css', inject: true},

      { src: `${this.CSS_SRC}/theme/layout/js/layout.js`, inject: true, vendor: false },
      { src: `${this.CSS_SRC}/theme/layout/js/perfect-scrollbar.js`, inject: true, vendor: false },

      //{ src: `${this.CSS_SRC}/font-awesome.min.css`, inject: true, vendor: true }
      // {src: `${this.APP_SRC}/your-path-to-lib/libs/jquery-ui.js`, inject: true, vendor: false}
      // {src: `${this.CSS_SRC}/path-to-lib/test-lib.css`, inject: true, vendor: false},
    ];

    /* Add to or override NPM module configurations: */
    // this.mergeObject(this.PLUGIN_CONFIGS['browser-sync'], { ghostMode: false });
  }

}
